# README #

1. Copy and edit the config `cp config.template.php config.php`
2. Fetch all once `php fetch_all.php`
3. Add cronjob to keep it up to date, I run `php fetch_week.php` daily
4. Use a view to see the collected data, like `php overtime.php`