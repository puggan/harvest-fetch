<?php

	// Load config
	require_once(__DIR__ . "/config.php");
	global $config;

	// Load curl-library
	require_once(__DIR__ . "/remote.php");
	global $remote_site;

	// Configure auth
	$base_url = "https://{$config->project}.harvestapp.com/daily/"; //{DAY_OF_THE_YEAR}/{YEAR}
	$remote_site->set_auth($config->user, $config->token);
	$remote_site->headers = array(
		'Accept: application/json',
		// 'Content-Type: application/json',
	);

	// connect to databse
	$db = new SQLite3($config->db);
	$db->exec("CREATE TABLE IF NOT EXISTS days(day date UNIQUE, paid float, unpaid float, expected float default {$config->hours});");

	// silence date-warnings
	date_default_timezone_set(@date_default_timezone_get());

	// get intervals
	$starttime = strtotime($config->start_date);
	$start_year = date("Y", $starttime);
	$start_day = date("z", $starttime);
	$end_year = date("Y");
	$end_day = date("z") + 1;

	// fetch times
	$times = array();
	foreach(range($start_year, $end_year) as $year)
	{
		$leap_year = date("L", mktime(0, 0, 0, 1, 1, "$year"));

		// get day interval for current year
		$year_start_day = (($year == $start_year) ? $start_day : 1);
		$year_end_day = (($year == $end_year) ? $end_day : 365 + $leap_year);

		// loop all days this year
		foreach(range($year_start_day, $year_end_day) as $day)
		{
			// Fetch from API
			$url = $base_url . $day . "/" . $year;
			$raw = $remote_site->get_page($url);
			$json = json_decode($raw);

			// Collect hours
			$day_times = array('paid' => 0, 'unpaid' => 0);
			foreach($json->day_entries as $entry)
			{
				if($entry->project_id == $config->unpaid_project_id)
				{
					$day_times['unpaid'] += $entry->hours;
				}
				else
				{
					$day_times['paid'] += $entry->hours;
				}
			}
			$day_total = array_sum($day_times);

			// if any times, save
			if($day_total)
			{
				$times[$json->for_day] = $day_times;

				// add a row for the date, if missing, then update it, like "INSERT INTO .. ON DUPLICATE KEY UPDATE .."
				$db->exec(sprintf("INSERT OR IGNORE INTO days(day) VALUES('%s')", $json->for_day));
				$db->exec(sprintf("UPDATE days SET paid = '%s', unpaid = '%s' WHERE day = '%s'", $day_times['paid'], $day_times['unpaid'], $json->for_day));
			}
		}
	}
