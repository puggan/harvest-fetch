<?php

	// Load config
	require_once(__DIR__ . "/config.php");
	global $config;

	date_default_timezone_set(@date_default_timezone_get());

	$db = new SQLite3($config->db);

	$total = 0;
	$last_week = NULL;
	$week_sums = array('work' => 0, 'expected' => 0);
	$last_month = NULL;
	$month_sums = array('work' => 0, 'expected' => 0);

	$resultset = $db->query("SELECT * FROM days ORDER BY day");
	while ($row = $resultset->fetchArray())
	{
		$timestamp = strtotime($row['day']);
		$this_week = date("o\\wW", $timestamp);
		$this_month = date("Y-m-xx", $timestamp);

		if($this_week != $last_week)
		{
			if($last_week)
			{
				printf("%s: %.1f of %.1f = %.1f (%.1f)\n\n", $last_week, $week_sums['work'], $week_sums['expected'], $week_sums['work'] - $week_sums['expected'], $total);
			}
			$week_sums = array('work' => 0, 'expected' => 0);
		}

		if($this_month != $last_month)
		{
			if($last_month)
			{
				printf("%s: %.1f of %.1f = %.1f (%.1f)\n\n\n", $last_month, $month_sums['work'], $month_sums['expected'], $month_sums['work'] - $month_sums['expected'], $total);
			}
			$month_sums = array('work' => 0, 'expected' => 0);
		}

		$work = $row['paid'] + $row['unpaid'];
		$overtime = $work - $row['expected'];
		$total += $overtime;
		printf("%s: %.1f of %.1f = %.1f (%.1f)\n", $row['day'], $work, $row['expected'], $overtime, $total);

		$last_week = $this_week;
		$week_sums['work'] += $work;
		$week_sums['expected'] += $row['expected'];
		$last_month = $this_month;
		$month_sums['work'] += $work;
		$month_sums['expected'] += $row['expected'];
	}

	if($last_week)
	{
		printf("%s: %.1f of %.1f = %.1f (%.1f)\n\n", $last_week, $week_sums['work'], $week_sums['expected'], $week_sums['work'] - $week_sums['expected'], $total);
	}

	if($last_month)
	{
		printf("%s: %.1f of %.1f = %.1f (%.1f)\n\n\n", $last_month, $month_sums['work'], $month_sums['expected'], $month_sums['work'] - $month_sums['expected'], $total);
	}

	echo PHP_EOL;